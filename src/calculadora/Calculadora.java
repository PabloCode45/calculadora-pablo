package calculadora;
import java.awt.Color;
import java.awt.Container;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Calculadora extends JFrame {

	public Calculadora() {
		//Esto crea una calculadora que se visualiz a trav�s de una ventana. Se pueden pulsar los botones aunque no funciona.
		
		this.setSize(350, 400);
		this.setTitle("Calculadora");
		this.setLocationRelativeTo(null);
		JPanel panel = new JPanel();
		panel.setBackground(new Color(36, 79, 74));
		panel.setLayout(null);
		this.getContentPane().add(panel);

		
		JButton boton7 = new JButton("7");
		boton7.setBounds(50, 100, 50 , 50);
		panel.add(boton7);
		
		JButton boton8 = new JButton("8");
		boton8.setBounds(110, 100, 50 , 50);
		panel.add(boton8);
		
		JButton boton9 = new JButton("9");
		boton9.setBounds(170, 100, 50 , 50);
		panel.add(boton9);
		
		JButton boton4 = new JButton("4");
		boton4.setBounds(50, 160, 50 , 50);
		panel.add(boton4);
		
		JButton boton5 = new JButton("5");
		boton5.setBounds(110, 160, 50 , 50);
		panel.add(boton5);
		
		JButton boton6 = new JButton("6");
		boton6.setBounds(170, 160, 50 , 50);
		panel.add(boton6);
		
		JButton boton1 = new JButton("1");
		boton1.setBounds(50, 220, 50 , 50);
		panel.add(boton1);
		
		JButton boton2 = new JButton("2");
		boton2.setBounds(110, 220, 50 , 50);
		panel.add(boton2);
		
		JButton boton3 = new JButton("3");
		boton3.setBounds(170, 220, 50 , 50);
		panel.add(boton3);
		
		
		JButton botoncoma = new JButton(",");
		botoncoma.setBounds(50, 280, 50 , 50);
		panel.add(botoncoma);
		
		JButton boton0 = new JButton("0");
		boton0.setBounds(110, 280, 50 , 50);
		panel.add(boton0);
		
		JButton botonigual = new JButton("=");
		botonigual.setBounds(170, 280, 50 , 50);
		panel.add(botonigual);
		
		JButton botonmas = new JButton("+");
		botonmas.setBounds(230, 100, 50 , 50);
		panel.add(botonmas);

		JButton botonmenos = new JButton("-");
		botonmenos.setBounds(230, 160, 50 , 50);
		panel.add(botonmenos);
		
		JButton botonmultiplicacion = new JButton("*");
		botonmultiplicacion.setBounds(230, 220, 50 , 50);
		panel.add(botonmultiplicacion);
		
		JButton botondivision = new JButton("/");
		botondivision.setBounds(230, 280, 50 , 50);
		panel.add(botondivision);
	}
	
	
	public static int suma(int uno, int dos) {		
		return uno+dos;
		
	}
	
	
	public static int resta(int uno, int dos) {
		return uno-dos;
		
	}
	
	
	public static int multiplicacion(int uno, int dos) {
		return uno*dos;
		
	}
	
	
	public static int division(int uno, int dos) {
		return uno/dos;
		
	}



}