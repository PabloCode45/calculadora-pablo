package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import calculadora.Calculadora;

class CalculadoraTest {

	@Test
	public void sumaTest() {
		assertEquals(3, Calculadora.suma(1, 2));
		
	}
	
	
	@Test
	public void restaTest() {		
		assertEquals(1, Calculadora.resta(2, 1));
		
	}
	
	
	@Test
	public void multiplicacionTest() {
		assertEquals(10, Calculadora.multiplicacion(2, 5));
		
	}
	
	
	@Test
	public void divisionTest() {		
		assertEquals(5, Calculadora.division(10, 2));
		
	}
	
}
